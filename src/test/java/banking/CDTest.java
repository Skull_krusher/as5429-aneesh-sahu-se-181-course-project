package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CDTest {
    private final int ID = 12345678;
    CD cd;

    @BeforeEach
    void setUp() {
        cd = new CD(ID, 0.5, 100);
    }

    @Test
    public void creating_a_cd() {
        CD cd = new CD(ID, 0.5, 1000);
        assertEquals(cd.getCash(), 1000);
    }

    @Test
    public void depositing_into_cd() {
        assertThrows(RuntimeException.class, () -> {
            cd.deposit(1000);
        });
    }

    @Test
    public void string_of_account() {
        assertEquals(cd.toString(), "Cd 12345678 100.00 0.50");
    }

    @Test
    public void string_of_account_two_changes() {
        cd.withdraw(20);
        assertEquals(cd.toString(), "Cd 12345678 80.00 0.50");
    }
}
