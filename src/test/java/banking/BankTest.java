package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class BankTest {

    public final int Id = 12345678;
    Bank bank;

    @BeforeEach
    void setUp() {
        bank = new Bank();
    }

    @Test
    public void creating_a_bank() {
        assertEquals(bank.getLength(), 0);
    }

    @Test
    public void adding_a_savings_account() {
        Savings saving = new Savings(Id, 0.9);
        bank.addAccount(saving);
        assertTrue(bank.hasAccount(Id));
        assertEquals(bank.getLength(), 1);
    }

    @Test
    public void adding_a_cd_account() {
        CD cd = new CD(Id, 0.9, 100);
        bank.addAccount(cd);
        assertTrue(bank.hasAccount(Id));
    }

    @Test
    public void adding_a_checking_account() {
        Checkings checking = new Checkings(Id, 0.9);
        bank.addAccount(checking);
        assertTrue(bank.hasAccount(Id));
    }

    @Test
    public void depositing_to_added_savings_account() {
        Savings saving = new Savings(Id, 0.9);
        bank.addAccount(saving);
        bank.deposit(Id, 400);
        assertEquals(bank.getCash(Id), 400);
    }

    @Test
    public void depositing_to_added_checking_account() {
        Checkings checking = new Checkings(Id, 0.9);
        bank.addAccount(checking);
        bank.deposit(Id, 400);
        assertEquals(bank.getCash(Id), 400);
    }

    @Test
    public void depositing_to_added_checking_account_low_boundary() {
        Checkings checking = new Checkings(Id, 0.9);
        bank.addAccount(checking);
        bank.deposit(Id, 0);
        assertEquals(bank.getCash(Id), 0);
    }

    @Test
    public void depositing_to_added_checking_account_high_boundary() {
        Checkings checking = new Checkings(Id, 0.9);
        bank.addAccount(checking);
        bank.deposit(Id, 1000);
        assertEquals(bank.getCash(Id), 1000);
    }

    @Test
    public void depositing_to_added_Cd_account() {
        CD cd = new CD(Id, 0.9, 100);
        bank.addAccount(cd);
        assertThrows(RuntimeException.class, () -> bank.deposit(Id, 400));


    }

    @Test
    public void deposit_into_unknown_id() {
        assertThrows(RuntimeException.class, () -> bank.deposit(Id, 400));
    }

    @Test
    public void withdraw_from_added_savings_account() {
        Savings saving = new Savings(Id, 0.9);
        bank.addAccount(saving);
        bank.deposit(Id, 400);
        bank.withdraw(Id, 200);
        assertEquals(bank.getCash(Id), 200);
    }

    @Test
    public void withdraw_from_added_checking_account() {
        Checkings checking = new Checkings(Id, 0.9);
        bank.addAccount(checking);
        bank.deposit(Id, 400);
        bank.withdraw(Id, 200);
        assertEquals(bank.getCash(Id), 200);
    }

    @Test
    public void withdraw_from_added_cd_account() {
        CD cd = new CD(Id, 0.9, 1000);
        bank.addAccount(cd);
        bank.withdraw(Id, 200);
        assertEquals(bank.getCash(Id), 800);
    }

    @Test
    public void withdraw_from_unadded_account() {
        assertThrows(RuntimeException.class, () -> bank.withdraw(Id, 400));
    }

    @Test
    public void remove_an_account() {
        Checkings checking = new Checkings(Id, 0.9);
        bank.addAccount(checking);
        bank.removeAccount(Id);
        assertThrows(RuntimeException.class, () -> bank.getCash(Id));

    }

    @Test
    public void apply_interest_one() {
        bank.addAccount(new Savings(12345678, 6));
        bank.deposit(12345678, 100);
        bank.applyInterest();
        assertEquals(bank.getCash(12345678), 100.5);
    }

    @Test
    public void apply_interest_many() {
        bank.addAccount(new Savings(12345678, 6));
        bank.addAccount(new Savings(87654321, 6));
        bank.addAccount(new Savings(12344321, 6));
        bank.deposit(12345678, 100);
        bank.deposit(87654321, 150);
        bank.deposit(12344321, 200);
        bank.applyInterest();
        assertEquals(bank.getCash(12345678), 100.5);
        assertEquals(bank.getCash(87654321), 150.75);
        assertEquals(bank.getCash(12344321), 201);
    }

    @Test
    public void remove_empty_accounts() {
        bank.addAccount(new Savings(12345678, 6));
        bank.addAccount(new Savings(87654321, 6));
        bank.addAccount(new Savings(12344321, 6));
        bank.removeEmptyAccount();
        assertFalse(bank.hasAccount(12345678));
        assertFalse(bank.hasAccount(87654321));
        assertFalse(bank.hasAccount(12344321));
    }

    @Test
    public void low_balance_deduction() {
        bank.addAccount(new Savings(12345678, 6));
        bank.deposit(12345678, 75);
        bank.lowBalanceDeduction();
        assertEquals(bank.getCash(12345678), 50);
    }

    @Test
    public void low_balance_deduction_many() {
        bank.addAccount(new Savings(12345678, 6));
        bank.addAccount(new Savings(87654321, 6));
        bank.addAccount(new Savings(12344321, 6));
        bank.deposit(12345678, 75);
        bank.deposit(87654321, 50);
        bank.deposit(12344321, 25);
        bank.lowBalanceDeduction();
        assertEquals(bank.getCash(12345678), 50);
        assertEquals(bank.getCash(87654321), 25);
        assertEquals(bank.getCash(12344321), 0);
    }

    @Test
    public void age_account() {
        bank.addAccount(new Savings(12345678, 6));
        bank.ageAccount();
        assertEquals(bank.getAge(12345678), 1);
    }

    @Test
    public void age_account_more_than_once() {
        bank.addAccount(new Savings(12345678, 6));
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        assertEquals(bank.getAge(12345678), 5);
    }

    @Test
    public void savings_withdraw_check_true() {
        bank.addAccount(new Savings(12345678, 6));
        assertTrue(bank.getWithdrawStatusSavings(12345678));
    }

    @Test
    public void savings_withdraw_check_false() {
        bank.addAccount(new Savings(12345678, 6));
        bank.setWithdrawStatusSavings(12345678, true);
        assertFalse(bank.getWithdrawStatusSavings(12345678));
    }

    @Test
    public void format_new_savings_account() {
        bank.addAccount(new Savings(12345678, 10));
        String output = bank.accountDetail(12345678);
        assertEquals(output, "Savings 12345678 0.00 10.00");
    }

    @Test
    public void format_savings_account() {
        bank.addAccount(new Savings(12345678, 10));
        bank.deposit(12345678, 300);
        String output = bank.accountDetail(12345678);
        assertEquals(output, "Savings 12345678 300.00 10.00");
    }

    @Test
    public void format_new_checking_account() {
        bank.addAccount(new Checkings(12345678, 10));
        String output = bank.accountDetail(12345678);
        assertEquals(output, "Checking 12345678 0.00 10.00");
    }

    @Test
    public void format_checking_account() {
        bank.addAccount(new Checkings(12345678, 10));
        bank.deposit(12345678, 300);
        String output = bank.accountDetail(12345678);
        assertEquals(output, "Checking 12345678 300.00 10.00");
    }

    @Test
    public void format_cd_account() {
        bank.addAccount(new CD(12345678, 10, 300));
        String output = bank.accountDetail(12345678);
        assertEquals(output, "Cd 12345678 300.00 10.00");
    }
}
