package banking;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class WithdrawValidationTest {
    WithdrawValidation withdraw = new WithdrawValidation();
    Bank bank = new Bank();

    @Test
    public void withdraw_from_active_account() {
        bank.addAccount(new Checkings(12345678, 2));
        bank.deposit(12345678, 400);
        bank.ageAccount();
        boolean res = withdraw.WithdrawCheck("Withdraw 12345678 200".split(" "), bank);
        assertTrue(res);
    }

    @Test
    public void withdraw_from_checking_account_boundary_too_high() {
        bank.addAccount(new Checkings(12345678, 2));
        bank.deposit(12345678, 500);
        bank.ageAccount();
        boolean res = withdraw.WithdrawCheck("Withdraw 12345678 500".split(" "), bank);
        assertFalse(res);
    }

    @Test
    public void withdraw_from_checking_account_boundary_high() {
        bank.addAccount(new Checkings(12345678, 2));
        bank.deposit(12345678, 500);
        bank.ageAccount();
        boolean res = withdraw.WithdrawCheck("Withdraw 12345678 400".split(" "), bank);
        assertTrue(res);
    }

    @Test
    public void withdraw_from_savings_account_valid_twice() {
        bank.addAccount(new Savings(12345678, 2));
        bank.deposit(12345678, 500);
        bank.ageAccount();
        bank.setWithdrawStatusSavings(12345678, true);
        boolean res = withdraw.WithdrawCheck("Withdraw 12345678 400".split(" "), bank);
        assertFalse(res);
    }

    @Test
    public void withdraw_from_savings_account_boundary_high() {
        bank.addAccount(new Savings(12345678, 2));
        bank.deposit(12345678, 1000);
        bank.ageAccount();
        boolean res = withdraw.WithdrawCheck("Withdraw 12345678 1000".split(" "), bank);
        assertTrue(res);
    }

    @Test
    public void withdraw_from_savings_account_boundary_too_high() {
        bank.addAccount(new Savings(12345678, 2));
        bank.deposit(12345678, 1100);
        bank.ageAccount();
        boolean res = withdraw.WithdrawCheck("Withdraw 12345678 1100".split(" "), bank);
        assertFalse(res);
    }

    @Test
    public void alpha_numeric_in_id() {
        bank.addAccount(new Savings(12345678, 2));
        bank.deposit(12345678, 1100);
        bank.ageAccount();
        boolean res = withdraw.WithdrawCheck("Withdraw 12A45678 1100".split(" "), bank);
        assertFalse(res);
    }

    @Test
    public void cd_withdraw_valid() {
        bank.addAccount(new CD(12345678, 6, 1000));
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        boolean res = withdraw.WithdrawCheck("Withdraw 12345678 1061.68".split(" "), bank);
        assertTrue(res);
    }

    @Test
    public void cd_withdraw_account_too_young() {
        bank.addAccount(new CD(12345678, 6, 1000));
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        boolean res = withdraw.WithdrawCheck("Withdraw 12345678 1100".split(" "), bank);
        assertFalse(res);
    }

    @Test
    public void cd_withdraw_below_balance() {
        bank.addAccount(new CD(12345678, 6, 1000));
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        boolean res = withdraw.WithdrawCheck("Withdraw 12345678 500".split(" "), bank);
        assertFalse(res);
    }

    @Test
    public void cd_withdraw_boundary() {
        bank.addAccount(new CD(12345678, 6, 1000));
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        boolean res = withdraw.WithdrawCheck("Withdraw 12345678 1000".split(" "), bank);
        assertTrue(res);
    }

    @Test
    public void cd_withdraw_boundary_age() {
        bank.addAccount(new CD(12345678, 6, 1000));
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        boolean res = withdraw.WithdrawCheck("Withdraw 12345678 1061.68".split(" "), bank);
        assertTrue(res);
    }


}
