package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SavingsTest {

    private final int ID = 12345678;
    Savings saving;

    @BeforeEach
    void setUp() {
        saving = new Savings(ID, 0.5);
    }

    @Test
    public void create_empty_savings_account() {
        assertEquals(saving.getCash(), 0);
    }

    @Test
    public void string_of_account() {
        saving.deposit(300);
        assertEquals(saving.toString(), "Savings 12345678 300.00 0.50");
    }

    @Test
    public void string_of_account_two_changes() {
        saving.deposit(300);
        saving.withdraw(200);
        assertEquals(saving.toString(), "Savings 12345678 100.00 0.50");
    }
}
