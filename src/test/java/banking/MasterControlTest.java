package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MasterControlTest {
    MasterControl masterControl;
    List<String> input;


    @BeforeEach
    public void setUp() {
        input = new ArrayList<>();
        Bank bank = new Bank();
        masterControl = new MasterControl(bank, new Validation(bank), new CommandProcessor(bank), new CommandStorage(bank));
    }

    @Test
    public void typo_in_create_command_is_invalid() {
        input.add("creat checking 12345678 1.0");

        List<String> actual = masterControl.start(input);
        assertSingleCommand("creat checking 12345678 1.0", actual);
    }

    @Test
    public void typo_in_deposit_is_invalid() {
        input.add("depositt 12345678 100");

        List<String> actual = masterControl.start(input);
        assertSingleCommand("depositt 12345678 100", actual);
    }

    private void assertSingleCommand(String command, List<String> actual) {
        assertEquals(1, actual.size());
        assertEquals(command, actual.get(0));
    }

    @Test
    public void two_typo_commands_both_invalid() {
        input.add("creat checking 12345678 1.0");
        input.add("depositt 12345678 100");

        List<String> actual = masterControl.start(input);

        assertEquals(2, actual.size());
        assertEquals("creat checking 12345678 1.0", actual.get(0));
        assertEquals("depositt 12345678 100", actual.get(1));
    }


    @Test
    public void sample_make_sure_this_passes_unchanged_or_you_will_fail() {
        input.add("Create savings 12345678 0.6");
        input.add("Deposit 12345678 700");
        input.add("Deposit 12345678 5000");
        input.add("creAte cHecKing 98765432 0.01");
        input.add("Deposit 98765432 300");
        input.add("Transfer 98765432 12345678 300");
        input.add("Pass 1");
        input.add("Create cd 23456789 1.2 2000");
        List<String> actual = masterControl.start(input);

        assertEquals(5, actual.size());
        assertEquals("Savings 12345678 1000.50 0.60", actual.get(0));
        assertEquals("Deposit 12345678 700", actual.get(1));
        assertEquals("Transfer 98765432 12345678 300", actual.get(2));
        assertEquals("Cd 23456789 2000.00 1.20", actual.get(3));
        assertEquals("Deposit 12345678 5000", actual.get(4));
    }

    @Test
    public void add_one_account_deposit() {
        input.add("Create checking 12345678 5");
        input.add("Deposit 12345678 300");
        List<String> actual = masterControl.start(input);
        assertEquals(2, actual.size());
        assertEquals("Checking 12345678 300.00 5.00", actual.get(0));
        assertEquals("Deposit 12345678 300", actual.get(1));
    }

    @Test
    public void add_two_account_deposit() {
        input.add("Create checking 12345678 5");
        input.add("Deposit 12345678 300");
        input.add("Create savings 12345677 5");
        input.add("Deposit 12345677 300");
        List<String> actual = masterControl.start(input);
        assertEquals(4, actual.size());
        assertEquals("Checking 12345678 300.00 5.00", actual.get(0));
        assertEquals("Deposit 12345678 300", actual.get(1));
        assertEquals("Savings 12345677 300.00 5.00", actual.get(2));
        assertEquals("Deposit 12345677 300", actual.get(3));
    }

    @Test
    public void add_two_account_deposit_then_pass_time_one_invalid() {
        input.add("Create checking 12345678 5");
        input.add("Deposit 12345678 300");
        input.add("Create savings 12345678 5");
        input.add("Create savings 12345677 5");
        input.add("Deposit 12345677 300");
        input.add("Pass 1");

        List<String> actual = masterControl.start(input);
        assertEquals(5, actual.size());
        assertEquals("Checking 12345678 301.25 5.00", actual.get(0));
        assertEquals("Deposit 12345678 300", actual.get(1));
        assertEquals("Savings 12345677 301.25 5.00", actual.get(2));
        assertEquals("Deposit 12345677 300", actual.get(3));
        assertEquals("Create savings 12345678 5", actual.get(4));
    }

    @Test
    public void add_savings_withdraw_twice_then_pass_then_withdraw() {
        input.add("Create savings 12345678 5");
        input.add("Deposit 12345678 2500");
        input.add("withdraw 12345678 200");
        input.add("withdraw 12345678 300");
        input.add("Pass 1");
        input.add("withdraw 12345678 200");

        List<String> actual = masterControl.start(input);
        assertEquals(5, actual.size());
        assertEquals("Savings 12345678 2109.58 5.00", actual.get(0));
        assertEquals("Deposit 12345678 2500", actual.get(1));
        assertEquals("withdraw 12345678 200", actual.get(2));
        assertEquals("withdraw 12345678 200", actual.get(3));
        assertEquals("withdraw 12345678 300", actual.get(4));
    }

}
