package banking;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CreateValidationTest {
    CreateValidation create = new CreateValidation();
    Bank bank = new Bank();

    @Test
    public void create_new_savings_account() {
        boolean result = create.CreateCheck("Create savings 87654321 0.6".split(" "), bank);
        assertTrue(result);
    }

    @Test
    public void create_no_apr_given() {
        boolean result = create.CreateCheck("Create savings 87654321".split(" "), bank);
        assertFalse(result);
    }

    @Test
    public void create_wrong_id() {
        boolean result = create.CreateCheck("Create savings 87651 0.6".split(" "), bank);
        assertFalse(result);
    }

    @Test
    public void two_accounts_of_same_id() {
        bank.addAccount(new Savings(87654321, 0.6));
        boolean result = create.CreateCheck("Create savings 87654321 0.6".split(" "), bank);
        assertFalse(result);
    }

    @Test
    public void apr_too_high() {
        boolean result = create.CreateCheck("Create savings 87654321 60".split(" "), bank);
        assertFalse(result);
    }

    @Test
    public void apr_too_low() {
        boolean result = create.CreateCheck("Create savings 87654321 -10".split(" "), bank);
        assertFalse(result);
    }

    @Test
    public void case_sensitivity() {
        boolean result = create.CreateCheck("CrEaTe savings 87654321 0.6".split(" "), bank);
        assertTrue(result);
    }

    @Test
    public void too_many_inputs() {
        boolean result = create.CreateCheck("Create savings 87654321 0.6 1000".split(" "), bank);
        assertFalse(result);
    }

    @Test
    public void too_many_inputs_cd() {
        boolean result = create.CreateCheck("Create CD 87654321 0.6 1000 102".split(" "), bank);
        assertFalse(result);
    }

    @Test
    public void create_new_checking_account() {
        boolean result = create.CreateCheck("Create checking 87654321 0.6".split(" "), bank);
        assertTrue(result);
    }

    @Test
    public void typo_in_account_type() {
        boolean result = create.CreateCheck("Create cheking 87654321 0.6".split(" "), bank);
        assertFalse(result);
    }

    @Test
    public void alpha_in_id() {
        boolean result = create.CreateCheck("Create checking 8765An21 0.6".split(" "), bank);
        assertFalse(result);
    }

    @Test
    public void alpha_in_apr() {
        boolean result = create.CreateCheck("Create checking 87654321 0.6AS".split(" "), bank);
        assertFalse(result);
    }

    @Test
    public void new_cd() {
        boolean result = create.CreateCheck("Create cd 87654321 0.6 1005".split(" "), bank);
        assertTrue(result);
    }

    @Test
    public void new_cd_value_too_high() {
        boolean result = create.CreateCheck("Create cd 87654321 0.6 100009".split(" "), bank);
        assertFalse(result);
    }

    @Test
    public void new_cd_value_too_low() {
        boolean result = create.CreateCheck("Create cd 87654321 0.6 100".split(" "), bank);
        assertFalse(result);
    }

    @Test
    public void edge_case_apr_bottom() {
        boolean result = create.CreateCheck("Create checking 87654321 0".split(" "), bank);
        assertTrue(result);
    }

    @Test
    public void edge_case_apr_top() {
        boolean result = create.CreateCheck("Create checking 87654321 10".split(" "), bank);
        assertTrue(result);
    }

    @Test
    public void edge_case_amount_top() {
        boolean result = create.CreateCheck("Create cd 87654321 0.6 10000".split(" "), bank);
        assertTrue(result);
    }

    @Test
    public void edge_case_amount_bottom() {
        boolean result = create.CreateCheck("Create cd 87654321 0.6 1000".split(" "), bank);
        assertTrue(result);
    }

    @Test
    public void new_cd_too_few_inputs() {
        boolean result = create.CreateCheck("Create cd 87654321 1005".split(" "), bank);
        assertFalse(result);
    }

    @Test
    public void new_cd_too_many_inputs() {
        boolean result = create.CreateCheck("Create cd 87654321 1005 10 21".split(" "), bank);
        assertFalse(result);
    }

    @Test
    public void illogical_input() {
        boolean result = create.CreateCheck("Jahanpanah, tusi great ho, taufa kabul karo".split(" "), bank);
        assertFalse(result);
    }


}
