package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AcountBaseTest {
    private final int ID = 12345678;
    accountbase ab;

    @BeforeEach
    void setUp() {
        ab = new Checkings(ID, 0.5);
    }

    @Test
    public void is_it_saving_the_id() {
        assertEquals(ab.getId(), ID);
    }

    @Test
    public void is_it_saving_the_apr() {
        assertEquals(ab.getApr(), 0.5);
    }

    @Test
    public void deposit_into_empty_account() {
        ab.deposit(100);
        assertEquals(ab.getCash(), 100);
    }

    @Test
    public void deposit_many_in_one_account() {
        ab.deposit(100);
        ab.deposit(300);
        assertEquals(ab.getCash(), 400);
    }

    @Test
    public void withdraw_from_account_once_not_zero() {
        ab.deposit(400);
        ab.withdraw(200);
        assertEquals(ab.getCash(), 200);
    }

    @Test
    public void withdraw_from_account_many_non_zero() {
        ab.deposit(400);
        ab.withdraw(100);
        ab.withdraw(100);
        assertEquals(ab.getCash(), 200);
    }

    @Test
    public void withdraw_from_account_negative() {

        ab.deposit(400);
        ab.withdraw(500);
        assertEquals(ab.getCash(), 0);

    }

    @Test
    public void applying_interest() {
        ab.deposit(100);
        ab.applyInterest();
        assertEquals(ab.getCash(), 100.5);
    }

    @Test
    public void withdraw_from_account_boundary() {
        ab.deposit(400);
        ab.withdraw(400);
        assertEquals(ab.getCash(), 0);
    }

    @Test
    public void get_age() {
        ab.age();
        assertEquals(ab.getAge(), 1);
    }

    @Test
    public void get_age_more_than_one() {
        ab.age();
        ab.age();
        ab.age();
        assertEquals(ab.getAge(), 3);
    }

}
