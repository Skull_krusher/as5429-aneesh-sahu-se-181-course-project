package banking;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class WithdrawProcessorTest {
    WithdrawProcessor withdraw = new WithdrawProcessor();
    Bank bank = new Bank();

    @Test
    public void withdraw_from_active_checking() {
        bank.addAccount(new Checkings(12345678, 8));
        bank.deposit(12345678, 1000);
        bank.ageAccount();
        withdraw.withdraw("withdraw 12345678 500".split(" "), bank);
        assertEquals(bank.getCash(12345678), 500);
    }

    @Test
    public void withdraw_from_active_savings() {
        bank.addAccount(new Savings(12345678, 8));
        bank.deposit(12345678, 1000);
        bank.ageAccount();
        withdraw.withdraw("withdraw 12345678 500".split(" "), bank);
        assertFalse(bank.getWithdrawStatusSavings(12345678));
        assertEquals(bank.getCash(12345678), 500);
    }

    @Test
    public void withdraw_from_active_cd() {
        bank.addAccount(new CD(12345678, 8, 1000));
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.ageAccount();
        bank.withdraw(12345678, 1000);
        assertEquals(bank.getCash(12345678), 0);
    }

}
