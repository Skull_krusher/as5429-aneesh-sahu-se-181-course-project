package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TransferProcessorTest {
    Bank bank;
    TransferProcessor processor;

    @BeforeEach
    public void setUp() {
        bank = new Bank();
        processor = new TransferProcessor();
    }

    @Test
    public void transfer_from_saving_to_saving() {
        bank.addAccount(new Savings(12345678, 5));
        bank.addAccount(new Savings(87654321, 5));
        bank.deposit(12345678, 300);
        processor.transfer("transfer 12345678 87654321 200".split(" "), bank);
        assertEquals(bank.getCash(12345678), 100);
        assertEquals(bank.getCash(87654321), 200);
    }

    @Test
    public void transfer_from_saving_to_checkings() {
        bank.addAccount(new Savings(12345678, 5));
        bank.addAccount(new Checkings(87654321, 5));
        bank.deposit(12345678, 300);
        processor.transfer("transfer 12345678 87654321 200".split(" "), bank);
        assertEquals(bank.getCash(12345678), 100);
        assertEquals(bank.getCash(87654321), 200);
    }

    @Test
    public void transfer_from_saving_to_saving_boundary_low() {
        bank.addAccount(new Savings(12345678, 5));
        bank.addAccount(new Savings(87654321, 5));
        bank.deposit(12345678, 300);
        processor.transfer("transfer 12345678 87654321 0".split(" "), bank);
        assertEquals(bank.getCash(12345678), 300);
        assertEquals(bank.getCash(87654321), 0);
    }

    @Test
    public void transfer_from_saving_to_saving_boundary_high() {
        bank.addAccount(new Savings(12345678, 5));
        bank.addAccount(new Savings(87654321, 5));
        bank.deposit(12345678, 300);
        processor.transfer("transfer 12345678 87654321 300".split(" "), bank);
        assertEquals(bank.getCash(12345678), 0);
        assertEquals(bank.getCash(87654321), 300);
    }

    @Test
    public void transfer_into_existing_savings() {
        bank.addAccount(new Savings(12345678, 5));
        bank.addAccount(new Savings(87654321, 5));
        bank.deposit(12345678, 300);
        bank.deposit(87654321, 300);
        processor.transfer("transfer 12345678 87654321 300".split(" "), bank);
        assertEquals(bank.getCash(12345678), 0);
        assertEquals(bank.getCash(87654321), 600);
    }
}
