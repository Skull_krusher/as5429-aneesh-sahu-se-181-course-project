package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class CommandProcessorTest {
    Bank bank;
    CommandProcessor processor;

    @BeforeEach
    public void setUp() {
        bank = new Bank();
        processor = new CommandProcessor(bank);
    }

    @Test
    public void deposit_many_times() {
        bank.addAccount(new Checkings(12345678, 5));
        processor.process("deposit 12345678 100");
        processor.process("deposit 12345678 100");
        assertEquals(bank.getCash(12345678), 200);
    }

    @Test
    public void create_and_deposit_together() {
        processor.process("create checking 12345678 1.0");
        processor.process("deposit 12345678 100");
        assertEquals(bank.getCash(12345678), 100);
    }

    @Test
    public void multiple_accounts_create_and_deposit() {
        processor.process("create checking 12345678 1.0");
        processor.process("deposit 12345678 100");
        processor.process("create savings 12345677 1.0");
        processor.process("deposit 12345677 100");
        assertEquals(bank.getCash(12345678), 100);
        assertEquals(bank.getCash(12345677), 100);
    }

    @Test
    public void transfer_success() {
        bank.addAccount(new Checkings(12345678, 5));
        processor.process("deposit 12345678 300");
        bank.addAccount(new Checkings(87654321, 5));
        processor.process("Transfer 12345678 87654321 200");
        assertEquals(bank.getCash(12345678), 100);
        assertEquals(bank.getCash(87654321), 200);
    }

    @Test
    public void passtime_success() {
        bank.addAccount(new Checkings(12345678, 5));
        processor.process("deposit 12345678 300");
        bank.addAccount(new Checkings(87654321, 5));
        processor.process("pass 1");
        assertFalse(bank.hasAccount(87654321));
        assertEquals(bank.getCash(12345678), 301.25);
    }

    @Test
    public void withdraw_success() {
        bank.addAccount(new Checkings(12345678, 5));
        bank.deposit(12345678, 200);
        processor.process("withdraw 12345678 200");
        assertEquals(bank.getCash(12345678), 0);
    }

    @Test
    public void account_deletion_due_to_0_balance() {
        processor.process("Create savings 12345678 0.6");
        processor.process("Deposit 12345678 700");
        processor.process("creAte cHecKing 98765432 0.01");
        processor.process("Deposit 98765432 300");
        processor.process("Transfer 98765432 12345678 300");
        processor.process("Pass 1");
        assertFalse(bank.hasAccount(98765432));
        assertEquals(bank.getCash(12345678), 1000.5);
    }
}
