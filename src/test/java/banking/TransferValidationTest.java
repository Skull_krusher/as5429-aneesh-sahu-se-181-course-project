package banking;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TransferValidationTest {
    TransferValidation transfer = new TransferValidation();
    Bank bank = new Bank();
    int ID1 = 12345678, ID2 = 87654321;

    @Test
    public void transfer_from_savings_to_savings() {
        bank.addAccount(new Savings(ID1, 0.5));
        bank.addAccount(new Savings(ID2, 0.5));
        bank.deposit(ID1, 300);
        Boolean result = transfer.TransferCheck("Transfer 12345678 87654321 200".split(" "), bank);
        assertTrue(result);
    }

    @Test
    public void transfer_from_savings_to_checking() {
        bank.addAccount(new Savings(ID1, 0.5));
        bank.addAccount(new Checkings(ID2, 0.5));
        bank.deposit(ID1, 300);
        Boolean result = transfer.TransferCheck("Transfer 12345678 87654321 200".split(" "), bank);
        assertTrue(result);
    }

    @Test
    public void transfer_from_savings_to_cd() {
        bank.addAccount(new Savings(ID1, 0.5));
        bank.addAccount(new CD(ID2, 0.5, 100));
        bank.deposit(ID1, 300);
        Boolean result = transfer.TransferCheck("Transfer 12345678 87654321 200".split(" "), bank);
        assertFalse(result);
    }

    @Test
    public void transfer_from_savings_to_savings_boundary_low() {
        bank.addAccount(new Savings(ID1, 0.5));
        bank.addAccount(new Savings(ID2, 0.5));
        bank.deposit(ID1, 300);
        Boolean result = transfer.TransferCheck("Transfer 12345678 87654321 300".split(" "), bank);
        assertTrue(result);
    }

    @Test
    public void transfer_from_savings_to_savings_more_than_in_bank() {
        bank.addAccount(new Savings(ID1, 0.5));
        bank.addAccount(new Savings(ID2, 0.5));
        bank.deposit(ID1, 300);
        Boolean result = transfer.TransferCheck("Transfer 12345678 87654321 400".split(" "), bank);
        assertFalse(result);
    }

    @Test
    public void transfer_no_such_id() {
        Boolean result = transfer.TransferCheck("Transfer 12345678 87654321 400".split(" "), bank);
        assertFalse(result);
    }

    @Test
    public void transfer_alpha_num_in_id() {
        Boolean result = transfer.TransferCheck("Transfer 12345A78 87B54321 400".split(" "), bank);
        assertFalse(result);
    }

    @Test
    public void transfer_from_savings_to_savings_zero() {
        bank.addAccount(new Savings(ID1, 0.5));
        bank.addAccount(new Savings(ID2, 0.5));
        bank.deposit(ID1, 300);
        Boolean result = transfer.TransferCheck("Transfer 12345678 87654321 0".split(" "), bank);
        assertTrue(result);
    }
}
