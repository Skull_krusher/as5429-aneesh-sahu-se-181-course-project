package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PassTimeProcessorTest {
    Bank bank;
    PasstimeProcessor processor;

    @BeforeEach
    public void setUp() {
        bank = new Bank();
        processor = new PasstimeProcessor();
    }

    @Test
    public void pass_time_add_interest() {
        bank.addAccount(new Savings(12345678, 6));
        bank.deposit(12345678, 100);
        processor.pass("Pass 1".split(" "), bank);
        assertEquals(bank.getCash(12345678), 100.5);
    }

    @Test
    public void pass_time_add_interest_on_two_accounts() {
        bank.addAccount(new Savings(12345678, 6));
        bank.addAccount(new Savings(87654321, 6));
        bank.deposit(12345678, 100);
        bank.deposit(87654321, 150);
        processor.pass("Pass 1".split(" "), bank);
        assertEquals(bank.getCash(12345678), 100.5);
        assertEquals(bank.getCash(87654321), 150.75);
    }

    @Test
    public void pass_time_remove_empty_account() {
        bank.addAccount(new Savings(12345678, 6));
        processor.pass("Pass 1".split(" "), bank);
        assertFalse(bank.hasAccount(12345678));
    }

    @Test
    public void pass_time_remove_many_empty_account() {
        bank.addAccount(new Savings(12345678, 6));
        bank.addAccount(new Savings(88888888, 6));
        bank.addAccount(new Savings(88888888, 6));
        bank.addAccount(new Savings(66666666, 6));
        processor.pass("Pass 1".split(" "), bank);
        assertFalse(bank.hasAccount(12345678));
        assertFalse(bank.hasAccount(88888888));
        assertFalse(bank.hasAccount(88888888));
        assertFalse(bank.hasAccount(66666666));
    }

    @Test
    public void pass_time_account_deduction_low_balance() {
        bank.addAccount(new Savings(12345678, 6));
        bank.deposit(12345678, 50);
        processor.pass("Pass 1".split(" "), bank);
        assertEquals(bank.getCash(12345678), 25.125);
    }

    @Test
    public void pass_time_account_deduction_low_balance_many_accounts() {
        bank.addAccount(new Savings(12345678, 6));
        bank.addAccount(new Savings(87654321, 6));
        bank.addAccount(new Savings(12344321, 6));
        bank.deposit(12345678, 50);
        bank.deposit(87654321, 25);
        bank.deposit(12344321, 5);
        processor.pass("Pass 1".split(" "), bank);
        assertEquals(bank.getCash(12345678), 25.125);
        assertEquals(bank.getCash(87654321), 0);
        assertEquals(bank.getCash(12344321), 0);
    }

    @Test
    public void pass_time_interest_over_multiple_months() {
        bank.addAccount(new Savings(12345678, 6));
        bank.deposit(12345678, 100);
        processor.pass("Pass 12".split(" "), bank);
        assertEquals(bank.getCash(12345678), 106.16778118644996);
    }

    @Test
    public void pass_time_deduction_over_multiple_months() {
        bank.addAccount(new Savings(12345678, 6));
        bank.deposit(12345678, 75);
        processor.pass("Pass 3".split(" "), bank);
        assertEquals(bank.getCash(12345678), 0.37813124999999886);
    }

    @Test
    public void pass_time_deduction_over_multiple_months_remove_account() {
        bank.addAccount(new Savings(12345678, 6));
        bank.deposit(12345678, 75);
        processor.pass("Pass 5".split(" "), bank);
        assertFalse(bank.hasAccount(12345678));
    }

    @Test
    public void is_it_aging() {
        bank.addAccount(new Savings(12345678, 6));
        bank.deposit(12345678, 100);
        processor.pass("Pass 1".split(" "), bank);
        assertEquals(bank.getAge(12345678), 1);
    }

    @Test
    public void is_it_aging_more_than_one() {
        bank.addAccount(new Savings(12345678, 6));
        bank.deposit(12345678, 100);
        processor.pass("Pass 1".split(" "), bank);
        assertEquals(bank.getAge(12345678), 1);
        processor.pass("Pass 3".split(" "), bank);
        assertEquals(bank.getAge(12345678), 4);
    }

    @Test
    public void withdraw_from_savings_pass_in_between() {
        bank.addAccount(new Savings(12345678, 6));
        bank.deposit(12345678, 1000);
        bank.setWithdrawStatusSavings(12345678, true);
        processor.pass("Pass 1".split(" "), bank);
        assertTrue(bank.getWithdrawStatusSavings(12345678));
    }


}
