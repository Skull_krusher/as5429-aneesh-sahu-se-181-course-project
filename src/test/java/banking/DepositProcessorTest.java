package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DepositProcessorTest {
    Bank bank;
    DepositProcessor processor;

    @BeforeEach
    public void setUp() {
        bank = new Bank();
        processor = new DepositProcessor();
    }

    @Test
    public void deposit_into_new_account_savings() {
        bank.addAccount(new Savings(12345678, 5));
        processor.deposit("deposit 12345678 100".split(" "), bank);
        assertEquals(bank.getCash(12345678), 100);
    }

    @Test
    public void deposit_into_new_account_checking() {
        bank.addAccount(new Checkings(12345678, 5));
        processor.deposit("deposit 12345678 100".split(" "), bank);
        assertEquals(bank.getCash(12345678), 100);
    }

    @Test
    public void deposit_into_existing_account_savings() {
        bank.addAccount(new Savings(12345678, 5));
        bank.deposit(12345678, 100);
        processor.deposit("deposit 12345678 100".split(" "), bank);
        assertEquals(bank.getCash(12345678), 200);
    }

    @Test
    public void deposit_into_existing_account_checking() {
        bank.addAccount(new Checkings(12345678, 5));
        bank.deposit(12345678, 100);
        processor.deposit("deposit 12345678 100".split(" "), bank);
        assertEquals(bank.getCash(12345678), 200);
    }
}
