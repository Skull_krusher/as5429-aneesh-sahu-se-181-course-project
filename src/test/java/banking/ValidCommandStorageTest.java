package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ValidCommandStorageTest {
    private ValidCommandStorage storeRight;

    @BeforeEach
    public void setUp() {
        storeRight = new ValidCommandStorage();
    }

    @Test
    public void add_a_command_create() {
        storeRight.store("create Savings 12345678 6");
        assertEquals(storeRight.length(), 1);
    }

    @Test
    public void add_two_commands_deposit() {
        storeRight.store("create Savings 12345678 6");
        storeRight.store("Deposit 12345678 1000");
        assertTrue(storeRight.output(12345678).contains("Deposit 12345678 1000") && storeRight.length() == 1 && storeRight.length(12345678) == 1);
    }

    @Test
    public void add_to_two_accounts_deposit_and_transfer() {
        storeRight.store("create Savings 12345678 6");
        storeRight.store("Deposit 12345678 1000");
        storeRight.store("create Savings 12345677 6");
        storeRight.store("transfer 12345678 12345677 200");
        assertTrue(storeRight.output(12345678).contains("Deposit 12345678 1000"));
        assertTrue(storeRight.output(12345678).contains("transfer 12345678 12345677 200"));
        assertTrue(storeRight.output(12345677).contains("transfer 12345678 12345677 200"));
        assertEquals(storeRight.length(), 2);
        assertEquals(storeRight.length(12345678), 2);
        assertEquals(storeRight.length(12345677), 1);
    }

    @Test
    public void add_two_commands_withdraw() {
        storeRight.store("create Savings 12345678 6");
        storeRight.store("withdraw 12345678 1000");
        assertTrue(storeRight.output(12345678).contains("withdraw 12345678 1000") && storeRight.length() == 1 && storeRight.length(12345678) == 1);
    }

    @Test
    public void add_account_false() {
        storeRight.store("create Savings 12345678 6");
        storeRight.store("withdraw 12345678 1000");
        assertFalse(storeRight.output(12345678).contains("create Savings 12345678 5") && storeRight.output(12345678).contains("withdraw 12345678 1000") && storeRight.length() == 1 && storeRight.length(12345678) == 2);
    }

    @Test
    public void add_to_two_accounts_id_check() {
        storeRight.store("create Savings 12345678 6");
        storeRight.store("Deposit 12345678 1000");
        storeRight.store("create Savings 12345677 6");
        storeRight.store("transfer 12345678 12345677 200");
        assertTrue(storeRight.GetAllAccountId().contains(12345678));
        assertTrue(storeRight.GetAllAccountId().contains(12345677));
    }

    @Test
    public void add_to_two_accounts_id_check_false() {
        storeRight.store("create Savings 12345678 6");
        storeRight.store("Deposit 12345678 1000");
        storeRight.store("create Savings 12345677 6");
        storeRight.store("transfer 12345678 12345677 200");
        assertTrue(storeRight.GetAllAccountId().contains(12345678));
        assertTrue(storeRight.GetAllAccountId().contains(12345677));
        assertFalse(storeRight.GetAllAccountId().contains(12345676));
    }
}
