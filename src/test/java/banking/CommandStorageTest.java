package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CommandStorageTest {
    CommandStorage store;
    Bank bank;

    @BeforeEach
    public void setUp() {
        bank = new Bank();
        store = new CommandStorage(bank);
    }

    @Test
    public void add_valid_command() {
        store.store("Create Savings 12345678 0.6", true);
        bank.addAccount(new Savings(12345678, 0.6));
        store.store("This is invalid", false);
        List<String> actual = store.output();
        assertEquals(actual.get(0), "Savings 12345678 0.00 0.60");
        assertEquals(actual.get(1), "This is invalid");

    }

    @Test
    public void add_two_valid_command() {
        store.store("Create Savings 12345678 0.6", true);
        bank.addAccount(new Savings(12345678, 0.6));
        store.store("This is invalid", false);
        store.store("Deposit 12345678 2000", true);
        bank.deposit(12345678, 2000);
        List<String> actual = store.output();
        assertEquals(actual.get(0), "Savings 12345678 2000.00 0.60");
        assertEquals(actual.get(1), "Deposit 12345678 2000");
        assertEquals(actual.get(2), "This is invalid");

    }

    @Test
    public void add_many_valid_command() {
        store.store("Create Savings 12345678 0.6", true);
        bank.addAccount(new Savings(12345678, 0.6));
        store.store("This is invalid", false);
        store.store("Deposit 12345678 2000", true);
        bank.deposit(12345678, 2000);
        store.store("Withdraw 12345678 200", true);
        bank.withdraw(12345678, 200);
        List<String> actual = store.output();
        assertEquals(actual.get(0), "Savings 12345678 1800.00 0.60");
        assertEquals(actual.get(1), "Deposit 12345678 2000");
        assertEquals(actual.get(2), "Withdraw 12345678 200");
        assertEquals(actual.get(3), "This is invalid");

    }

    @Test
    public void add_many_valid_command_many_accounts() {
        store.store("Create Savings 12345678 0.6", true);
        bank.addAccount(new Savings(12345678, 0.6));
        store.store("This is invalid", false);
        store.store("Deposit 12345678 2000", true);
        bank.deposit(12345678, 2000);
        store.store("Withdraw 12345678 200", true);
        bank.withdraw(12345678, 200);

        store.store("Create Checking 12345677 0.6", true);
        bank.addAccount(new Checkings(12345677, 0.6));
        store.store("This is also invalid", false);
        store.store("Deposit 12345677 3000", true);
        bank.deposit(12345677, 3000);
        store.store("Withdraw 12345677 300", true);
        bank.withdraw(12345677, 300);

        List<String> actual = store.output();
        assertEquals(actual.get(0), "Savings 12345678 1800.00 0.60");
        assertEquals(actual.get(1), "Deposit 12345678 2000");
        assertEquals(actual.get(2), "Withdraw 12345678 200");

        assertEquals(actual.get(3), "Checking 12345677 2700.00 0.60");
        assertEquals(actual.get(4), "Deposit 12345677 3000");
        assertEquals(actual.get(5), "Withdraw 12345677 300");


        assertEquals(actual.get(6), "This is invalid");
        assertEquals(actual.get(7), "This is also invalid");

    }

    @Test
    public void add_many_valid_command_many_accounts_with_one_account_being_deleted() {
        store.store("Create Savings 12345678 0.6", true);
        bank.addAccount(new Savings(12345678, 0.6));
        store.store("This is invalid", false);
        store.store("Deposit 12345678 2000", true);
        bank.deposit(12345678, 2000);
        store.store("Withdraw 12345678 2000", true);
        bank.withdraw(12345678, 2000);

        store.store("Create Checking 12345677 0.6", true);
        bank.addAccount(new Checkings(12345677, 0.6));
        store.store("This is also invalid", false);
        store.store("Deposit 12345677 3000", true);
        bank.deposit(12345677, 3000);
        store.store("Withdraw 12345677 300", true);
        bank.withdraw(12345677, 300);
        bank.lowBalanceDeduction();
        bank.removeEmptyAccount();

        List<String> actual = store.output();

        assertEquals(actual.get(0), "Checking 12345677 2700.00 0.60");
        assertEquals(actual.get(1), "Deposit 12345677 3000");
        assertEquals(actual.get(2), "Withdraw 12345677 300");


        assertEquals(actual.get(3), "This is invalid");
        assertEquals(actual.get(4), "This is also invalid");

    }


}
