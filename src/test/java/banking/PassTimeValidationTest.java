package banking;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PassTimeValidationTest {
    PassTimeValidation pastime = new PassTimeValidation();
    Bank bank = new Bank();

    @Test
    public void pas_time() {
        boolean res = pastime.PasTimeCheck("Pass 4".split(" "));
        assertTrue(res);
    }

    @Test
    public void pas_time_boundary_low() {
        boolean res = pastime.PasTimeCheck("Pass 1".split(" "));
        assertTrue(res);
    }

    @Test
    public void pas_time_boundary_high() {
        boolean res = pastime.PasTimeCheck("Pass 60".split(" "));
        assertTrue(res);
    }

    @Test
    public void pas_time_too_low() {
        boolean res = pastime.PasTimeCheck("Pass 0".split(" "));
        assertFalse(res);
    }

    @Test
    public void pas_time_too_high() {
        boolean res = pastime.PasTimeCheck("Pass 61".split(" "));
        assertFalse(res);
    }
}
