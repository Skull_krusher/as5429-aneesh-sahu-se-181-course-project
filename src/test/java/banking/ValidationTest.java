package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ValidationTest {
    Validation validation;
    Bank bank;

    @BeforeEach
    public void setUp() {
        bank = new Bank();
        validation = new Validation(bank);
    }

    @Test
    public void case_sensitivity_create() {
        boolean result = validation.validate("CrEaTe savings 87654321 0.6");
        assertTrue(result);
    }

    @Test
    public void just_create() {
        boolean result = validation.validate("Create");
        assertFalse(result);
    }

    @Test
    public void typo_in_create() {
        boolean result = validation.validate("Craete savings 87654321 0.6");
        assertFalse(result);
    }

    @Test
    public void case_sensitivity_deposit() {
        bank.addAccount(new Checkings(87654321, 0.6));
        boolean result = validation.validate("DEpOsIt 87654321 10");
        assertTrue(result);
    }

    @Test
    public void no_amount_given() {
        bank.addAccount(new Checkings(87654321, 0.6));
        boolean result = validation.validate("Deposit 87654321");
        assertFalse(result);
    }

    @Test
    public void just_deposit() {
        bank.addAccount(new Checkings(87654321, 0.6));
        boolean result = validation.validate("Deposit");
        assertFalse(result);
    }

    @Test
    public void typo_in_deposit() {
        bank.addAccount(new Checkings(87654321, 0.6));
        boolean result = validation.validate("Deposet 87654321 100");
        assertFalse(result);
    }

    @Test
    public void deposit_fail() {
        bank.addAccount(new Checkings(87654321, 0.6));
        boolean result = validation.validate("Deposit 87621 100");
        assertFalse(result);
    }

    @Test
    public void invalid_id() {
        bank.addAccount(new Checkings(87654321, 0.6));
        boolean result = validation.validate("Deposit 87615321 100");
        assertFalse(result);
    }

    @Test
    public void just_transfer() {
        boolean result = validation.validate("Transfer");
        assertFalse(result);
    }

    @Test
    public void just_pass() {
        boolean result = validation.validate("Pass");
        assertFalse(result);
    }

    @Test
    public void transfer_success() {
        bank.addAccount(new Checkings(87654321, 0.6));
        bank.addAccount(new Checkings(12345678, 0.6));
        bank.deposit(12345678, 300);
        boolean res = validation.validate("Transfer 12345678 87654321 200");
        assertTrue(res);

    }

    @Test
    public void transfer_fail() {
        bank.addAccount(new Checkings(87654321, 0.6));
        bank.addAccount(new Checkings(12345678, 0.6));
        boolean res = validation.validate("Transfer 12345678 87654321 200");
        assertFalse(res);

    }

    @Test
    public void pass_success() {
        bank.addAccount(new Checkings(87654321, 0.6));
        boolean res = validation.validate("Pass 2");
        assertTrue(res);
    }

    @Test
    public void pass_fail() {
        bank.addAccount(new Checkings(87654321, 0.6));
        boolean res = validation.validate("Pass 64");
        assertFalse(res);
    }

    @Test
    public void withdraw_from_active_account_pass() {
        bank.addAccount(new Checkings(12345678, 2));
        bank.deposit(12345678, 400);
        bank.ageAccount();
        boolean res = validation.validate("Withdraw 12345678 200");
        assertTrue(res);
    }

    @Test
    public void withdraw_from_active_account_fail() {
        bank.addAccount(new Checkings(12345678, 2));
        bank.deposit(12345678, 400);
        bank.ageAccount();
        boolean res = validation.validate("Withdraw 12345678");
        assertFalse(res);
    }
}
