package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class InvalidCommandStorageTest {
    private InvalidCommandStorage storeWrong;

    @BeforeEach
    public void setUp() {
        storeWrong = new InvalidCommandStorage();
    }

    @Test
    public void add_a_command() {
        storeWrong.store("create this is a wrong command.");
        assertTrue(storeWrong.output().contains("create this is a wrong command."));
    }

    @Test
    public void add_multiple_commands() {
        storeWrong.store("create this is a wrong command.");
        storeWrong.store("create this is also a wrong command.");
        storeWrong.store("deposit, this is a correct command.  LOl, no its not");
        storeWrong.store("Guess what, this is again a wrong command");
        assertTrue(storeWrong.output().contains("create this is a wrong command.") && storeWrong.output().contains("create this is also a wrong command.") && storeWrong.output().contains("deposit, this is a correct command.  LOl, no its not") && storeWrong.output().contains("Guess what, this is again a wrong command"));
    }

    @Test
    public void start_from_empty_list() {
        storeWrong.store("create this is a wrong command.");
        assertTrue(storeWrong.output().contains("create this is a wrong command.") && storeWrong.length() == 1);

    }
}
