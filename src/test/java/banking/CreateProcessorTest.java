package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class CreateProcessorTest {
    Bank bank;
    CreateProcessor processor;

    @BeforeEach
    public void setUp() {
        bank = new Bank();
        processor = new CreateProcessor();
    }

    @Test
    public void create_savings_account() {
        processor.create("create savings 12345678 1.0".split(" "), bank);
        assertTrue(bank.hasAccount(12345678));
    }

    @Test
    public void create_checking_account() {
        processor.create("create checking 12345678 1.0".split(" "), bank);
        assertTrue(bank.hasAccount(12345678));
    }

    @Test
    public void create_cd_account() {
        processor.create("Create cd 12345678 1.2 2000".split(" "), bank);
        assertTrue(bank.hasAccount(12345678));
    }

}
