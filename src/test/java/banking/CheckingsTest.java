package banking;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CheckingsTest {

    private final int ID = 12345678;
    Checkings checking;

    @BeforeEach
    void setUp() {
        checking = new Checkings(ID, 0.5);
    }

    @Test
    public void create_empty_checking_account() {
        assertEquals(checking.getCash(), 0);
    }

    @Test
    public void string_of_account() {
        checking.deposit(300);
        assertEquals(checking.toString(), "Checking 12345678 300.00 0.50");
    }

    @Test
    public void string_of_account_two_changes() {
        checking.deposit(300);
        checking.withdraw(200);
        assertEquals(checking.toString(), "Checking 12345678 100.00 0.50");
    }
}
