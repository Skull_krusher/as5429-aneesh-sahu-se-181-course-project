package banking;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DepositeValidationTest {

    DepositeValidation deposit = new DepositeValidation();
    Bank bank = new Bank();

    @Test
    public void deposit_into_savings() {
        bank.addAccount(new Savings(87654321, 0.6));
        boolean result = deposit.DepositCheck("Deposit 87654321 10".split(" "), bank);
        assertTrue(result);
    }

    @Test
    public void deposit_into_checking() {
        bank.addAccount(new Checkings(87654321, 0.6));
        boolean result = deposit.DepositCheck("Deposit 87654321 10".split(" "), bank);
        assertTrue(result);
    }

    @Test
    public void deposit_into_cd() {
        bank.addAccount(new CD(87654321, 0.6, 1000));
        boolean result = deposit.DepositCheck("Deposit 87654321 10".split(" "), bank);
        assertFalse(result);
    }

    @Test
    public void case_sensitivity_deposit() {
        bank.addAccount(new Checkings(87654321, 0.6));
        boolean result = deposit.DepositCheck("DEpOsIt 87654321 10".split(" "), bank);
        assertTrue(result);
    }

    @Test
    public void deposit_into_non_existent_id() {
        boolean result = deposit.DepositCheck("Deposit 87654321 10".split(" "), bank);
        assertFalse(result);
    }

    @Test
    public void id_too_short() {
        bank.addAccount(new Checkings(8765, 0.6));
        boolean result = deposit.DepositCheck("Deposit 8765 100".split(" "), bank);
        assertFalse(result);
    }

    @Test
    public void alpha_in_id_deposit() {
        boolean result = deposit.DepositCheck("Deposit Aneehs 120".split(" "), bank);
        assertFalse(result);
    }


    @Test
    public void edge_case_amount_bottom_deposit() {
        bank.addAccount(new Savings(87654321, 0.6));
        boolean result = deposit.DepositCheck("Deposit 87654321 0".split(" "), bank);
        assertTrue(result);
    }

    @Test
    public void edge_case_amount_top_savings_deposit() {
        bank.addAccount(new Savings(87654321, 0.6));
        boolean result = deposit.DepositCheck("Deposit 87654321 2500".split(" "), bank);
        assertTrue(result);
    }

    @Test
    public void edge_case_amount_top_checking_deposit() {
        bank.addAccount(new Checkings(87654321, 0.6));
        boolean result = deposit.DepositCheck("Deposit 87654321 1000".split(" "), bank);
        assertTrue(result);
    }

    @Test
    public void amount_too_high_savings_deposit() {
        bank.addAccount(new Savings(87654321, 0.6));
        boolean result = deposit.DepositCheck("Deposit 87654321 25000".split(" "), bank);
        assertFalse(result);
    }

    @Test
    public void amount_too_high_checking_deposit() {
        bank.addAccount(new Checkings(87654321, 0.6));
        boolean result = deposit.DepositCheck("Deposit 87654321 10000".split(" "), bank);
        assertFalse(result);
    }

    @Test
    public void negative_amount_deposit() {
        bank.addAccount(new Checkings(87654321, 0.6));
        boolean result = deposit.DepositCheck("Deposit 87654321 -100".split(" "), bank);
        assertFalse(result);
    }

    @Test
    public void Alpha_in_amount_deposit() {
        bank.addAccount(new Checkings(87654321, 0.6));
        boolean result = deposit.DepositCheck("Deposit 87654321 ABC".split(" "), bank);
        assertFalse(result);
    }


}
