package banking;

public class WithdrawProcessor {
    protected void withdraw(String[] inp, Bank bank) {
        int id = Integer.parseInt(inp[1]);
        double cash = Double.parseDouble(inp[2]);
        bank.withdraw(id, cash);

    }
}
