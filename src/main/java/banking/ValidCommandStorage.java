package banking;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class ValidCommandStorage implements Storage {

    private Hashtable godown;
    private List<Integer> ID;

    ValidCommandStorage() {
        godown = new Hashtable();
        ID = new ArrayList<>();
    }

    @Override
    public void store(String str) {
        String[] inp = str.split(" ");
        if (inp[0].equalsIgnoreCase("create")) {
            addNewAccount(inp);
        } else {
            addToExistingAccount(inp, str);
        }
    }

    private void addNewAccount(String[] inp) {
        godown.put(Integer.parseInt(inp[2]), new ArrayList<>());
        ID.add(Integer.parseInt(inp[2]));

    }

    private void addToExistingAccount(String[] inp, String str) {

        if (inp[0].equalsIgnoreCase("deposit") || inp[0].equalsIgnoreCase("withdraw")) {
            ArrayList current = (ArrayList) godown.get(Integer.parseInt(inp[1]));
            current.add(str);
        } else if (inp[0].equalsIgnoreCase("Transfer")) {
            ArrayList current = (ArrayList) godown.get(Integer.parseInt(inp[1]));
            current.add(str);
            ArrayList other = (ArrayList) godown.get(Integer.parseInt(inp[2]));
            other.add(str);
        }

    }

    public List<String> output(int id) {
        return (ArrayList) godown.get(id);
    }

    public List GetAllAccountId() {
        return ID;
    }

    @Override
    public int length() {
        return godown.size();
    }

    public int length(int id) {
        ArrayList current = (ArrayList) godown.get(id);
        return current.size();
    }

}
