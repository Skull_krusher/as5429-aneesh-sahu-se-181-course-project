package banking;

import java.util.ArrayList;
import java.util.List;

public class CommandStorage {
    Bank bank;
    InvalidCommandStorage godownWrong;
    ValidCommandStorage godownRight;

    CommandStorage(Bank bank) {
        this.bank = bank;
        godownRight = new ValidCommandStorage();
        godownWrong = new InvalidCommandStorage();
    }

    public void store(String Input, boolean isCorrect) {
        if (isCorrect) {
            godownRight.store(Input);
        } else {
            godownWrong.store(Input);
        }
    }

    public List<String> output() {
        List<String> output = new ArrayList<>();

        List keys = godownRight.GetAllAccountId();

        for (int i = 0; i < keys.size(); i++) {
            int id = (int) keys.get(i);
            if (bank.hasAccount(id)) {
                output.add(bank.accountDetail(id));
                output.addAll(godownRight.output(id));
            }
        }
        output.addAll(godownWrong.output());
        return output;
    }


}
