package banking;

public interface Storage {

    void store(String str);

    int length();

}
