package banking;

public class WithdrawValidation extends ValidationCommons {

    protected boolean WithdrawCheck(String[] parts, Bank bank) {
        return lengthChecker(parts) && IDChecker(parts[1]) && CheckIfNumeric(parts[2]) && BankHasAccount(parts[1], bank) && valueCheck(parts, bank);
    }

    private boolean lengthChecker(String[] parts) {
        return parts.length == 3;
    }

    private boolean valueCheck(String[] parts, Bank bank) {
        return bank.isWithdrawValid(Integer.parseInt(parts[1]), Double.parseDouble(parts[2]));
    }
}
