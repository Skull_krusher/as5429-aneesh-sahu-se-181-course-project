package banking;

public class CD extends accountbase {
    CD(int id, double apr, double cash) {
        super(id, apr, cash);
    }

    @Override
    public void deposit(double amount) {
        throw new RuntimeException("no deposit into cd");
    }

    @Override
    public String toString() {
        return "Cd " + super.toString();
    }
}
