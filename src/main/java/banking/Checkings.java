package banking;

public class Checkings extends accountbase {

    Checkings(int id, double apr) {
        super(id, apr, 0);
    }

    @Override
    public String toString() {
        return "Checking " + super.toString();
    }
}
