package banking;

import java.util.ArrayList;
import java.util.List;

public class InvalidCommandStorage implements Storage {

    private List<String> godown;

    InvalidCommandStorage() {
        godown = new ArrayList<>();
    }

    @Override
    public void store(String str) {
        godown.add(str);
    }

    public List<String> output() {
        return godown;
    }

    @Override
    public int length() {
        return godown.size();
    }

}
