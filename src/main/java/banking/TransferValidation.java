package banking;

public class TransferValidation extends ValidationCommons {

    protected boolean TransferCheck(String[] byparts, Bank bank) {
        return lengthChecker(byparts) && IDChecker(byparts[1]) && IDChecker(byparts[2]) && BankHasAccount(byparts[1], bank) && CheckIfNumeric(byparts[3]) && BankHasAccount(byparts[2], bank) && CheckCash(byparts[1], byparts[3], bank) && bank.NoCd(Integer.parseInt(byparts[1]), Integer.parseInt(byparts[2]));
    }

    private boolean lengthChecker(String[] byparts) {
        return 4 == byparts.length;
    }
}
