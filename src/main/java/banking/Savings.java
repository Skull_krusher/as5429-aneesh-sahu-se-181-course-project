package banking;

public class Savings extends accountbase {
    boolean withdrawPerMonth;

    Savings(int id, double apr) {
        super(id, apr, 0);
        this.withdrawPerMonth = false;
    }

    public boolean IsWithdrawAvailable() {
        return !withdrawPerMonth;
    }

    public void SetWithdrawPerMonth(boolean inp) {
        this.withdrawPerMonth = inp;
    }

    @Override
    public String toString() {
        return "Savings " + super.toString();
    }
}
