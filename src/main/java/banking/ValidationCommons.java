package banking;

abstract public class ValidationCommons {


    protected boolean IDChecker(String byparts) {
        return byparts.length() == 8 && CheckIfNumeric(byparts); // to check Id is only numeric.

    }

    protected boolean CheckIfNumeric(String bypart) {
        return (bypart.toLowerCase().equals(bypart)) && (bypart.toUpperCase().equals(bypart));
    }

    protected boolean noDuplicateId(String[] byparts, Bank bank) {
        return !bank.hasAccount(Integer.parseInt(byparts[2]));
    }

    protected boolean AprChecker(String[] byparts) {
        if (CheckIfNumeric(byparts[3])) {
            double apr = Double.parseDouble(byparts[3]);
            return apr >= 0 && apr <= 10;
        } else {
            return false;
        }

    }

    protected boolean CdCreateAmountChecker(String[] parts) {
        double amount = Double.parseDouble(parts[4]);
        return amount >= 1000 && amount <= 10000;
    }

    protected boolean BankHasAccount(String ID, Bank bank) {
        if (IDChecker(ID)) {
            int id = Integer.parseInt(ID);
            return bank.hasAccount(id);
        } else {
            return false;
        }
    }

    protected boolean CheckCash(String ID, String cash, Bank bank) {
        double cashInBank = bank.getCash(Integer.parseInt(ID));
        double cashToMove = Double.parseDouble(cash);
        if (cashInBank < cashToMove) {
            return false;
        } else {
            return true;
        }
    }
}
