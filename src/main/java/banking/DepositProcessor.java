package banking;

public class DepositProcessor {
    protected void deposit(String[] inp, Bank bank) {
        int id = Integer.parseInt(inp[1]);
        double cash = Double.parseDouble(inp[2]);
        bank.deposit(id, cash);
    }
}
