package banking;

import java.util.Enumeration;
import java.util.Hashtable;

public class Bank {
    public Hashtable accounts;

    public Bank() {
        accounts = new Hashtable();
    }

    public int getLength() {
        return accounts.size();
    }

    public void addAccount(accountbase account) {
        accounts.put(account.getId(), account);

    }

    public double getCash(int id) {
        accountbase res = (accountbase) accounts.get(id);
        if (res == null) {
            throw new RuntimeException("no account found");
        } else {
            return res.getCash();
        }
    }

    public int getAge(int id) {
        accountbase res = (accountbase) accounts.get(id);
        return res.getAge();
    }


    public Boolean hasAccount(int Id) {
        accountbase res = (accountbase) accounts.get(Id);
        return res != null;

    }


    public void deposit(int id, double amount) {

        accountbase res = (accountbase) accounts.get(id);
        if (res == null) {
            throw new RuntimeException("no account exists");
        } else {
            res.deposit(amount);
        }
    }


    public void withdraw(int id, double amount) {
        accountbase res = (accountbase) accounts.get(id);
        if (res == null) {
            throw new RuntimeException("no account exists");
        } else {
            res.withdraw(amount);
            if (isSavings(id)) {
                setWithdrawStatusSavings(id, true);
            }
        }

    }

    public void removeAccount(int id) {
        accounts.remove(id);
    }

    public void removeEmptyAccount() {
        Enumeration<Integer> keys = accounts.keys();
        while (keys.hasMoreElements()) {
            int id = keys.nextElement();
            if (getCash(id) == 0) {
                removeAccount(id);
            }
        }
    }

    public boolean IsDepositValid(int id, Double amount) {
        accountbase res = (accountbase) accounts.get(id);

        if (res instanceof Savings) {
            return amount <= 2500 && amount >= 0;
        } else if (res instanceof Checkings) {
            return amount <= 1000 && amount >= 0;
        }
        return false;
    }

    public boolean NoCd(int id1, int id2) {
        accountbase acc1 = (accountbase) accounts.get(id1);
        accountbase acc2 = (accountbase) accounts.get(id2);
        return (!(acc1 instanceof CD) && !(acc2 instanceof CD));
    }

    private boolean isSavings(int id) {
        accountbase acc = (accountbase) accounts.get(id);
        return acc instanceof Savings;
    }

    public void transfer(int id1, int id2, double cash) {
        withdraw(id1, cash);
        deposit(id2, cash);
    }

    public void applyInterest() {
        Enumeration<Integer> keys = accounts.keys();
        while (keys.hasMoreElements()) {
            int id = keys.nextElement();
            addInterest(id);
        }
    }

    private void addInterest(int id) {
        accountbase res = (accountbase) accounts.get(id);
        double interest = res.cash * res.apr / (100 * 12);
        res.deposit(interest);
    }

    public void lowBalanceDeduction() {
        Enumeration<Integer> keys = accounts.keys();
        while (keys.hasMoreElements()) {
            int id = keys.nextElement();
            if (getCash(id) < 100) {
                withdraw(id, 25);
            }
        }
    }

    public void ageAccount() {
        Enumeration<Integer> keys = accounts.keys();
        while (keys.hasMoreElements()) {
            int id = keys.nextElement();
            accountbase res = (accountbase) accounts.get(id);
            res.age();
            if (res instanceof Savings) {
                ((Savings) res).SetWithdrawPerMonth(false);
            }
        }
    }

    public boolean isWithdrawValid(int ID, double Cash) {
        accountbase res = (accountbase) accounts.get(ID);
        if (res instanceof Savings) {
            return Cash <= 1000 && getWithdrawStatusSavings(ID);
        } else if (res instanceof Checkings) {
            return Cash <= 400;
        } else if (res instanceof CD) {
            return Cash >= res.getCash() && res.getAge() >= 12;
        }
        return false;
    }

    public boolean getWithdrawStatusSavings(int id) {
        Savings res = (Savings) accounts.get(id);
        return res.IsWithdrawAvailable();
    }

    public void setWithdrawStatusSavings(int id, boolean status) {
        Savings res = (Savings) accounts.get(id);
        res.SetWithdrawPerMonth(status);
    }

    public String accountDetail(int id) {
        accountbase res = (accountbase) accounts.get(id);
        return res.toString();
    }
}
