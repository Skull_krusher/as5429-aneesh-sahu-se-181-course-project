package banking;

import java.math.RoundingMode;
import java.text.DecimalFormat;

abstract public class accountbase {
    protected double cash;
    protected int Id;
    protected double apr;
    protected int age;

    accountbase(int idd, double aprr, double cash) {
        this.Id = idd;
        this.apr = aprr;
        this.cash = cash;
        this.age = 0;
    }

    public double getCash() {
        return cash;
    }

    public int getId() {
        return Id;
    }

    public double getApr() {
        return apr;
    }

    public void deposit(double amount) {
        this.cash += amount;
    }

    public void withdraw(double amount) {
        if (this.cash < amount) {
            this.cash = 0;
        } else {
            this.cash = this.cash - amount;
        }
    }

    public void applyInterest() {
        this.cash = this.cash + this.cash * this.apr / 100;
    }

    public void age() {
        age = age + 1;
    }

    public int getAge() {
        return this.age;
    }

    @Override
    public String toString() {
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        decimalFormat.setRoundingMode(RoundingMode.FLOOR);
        return Id + " " + decimalFormat.format(getCash()) + " " + decimalFormat.format(getApr());
    }
}
