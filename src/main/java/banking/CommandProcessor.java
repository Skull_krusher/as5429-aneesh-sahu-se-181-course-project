package banking;

public class CommandProcessor {
    Bank bank;
    CreateProcessor create;
    DepositProcessor deposit;
    TransferProcessor transfer;
    PasstimeProcessor pass;
    WithdrawProcessor withdraw;

    public CommandProcessor(Bank bank) {
        this.bank = bank;
        create = new CreateProcessor();
        deposit = new DepositProcessor();
        transfer = new TransferProcessor();
        pass = new PasstimeProcessor();
        withdraw = new WithdrawProcessor();
    }

    public void process(String input) {
        input = input.stripTrailing();
        String[] inp = input.split(" ");
        if (inp[0].equalsIgnoreCase("create")) {
            create.create(inp, bank);
        } else if (inp[0].equalsIgnoreCase("deposit")) {
            deposit.deposit(inp, bank);
        } else if (inp[0].equalsIgnoreCase("transfer")) {
            transfer.transfer(inp, bank);
        } else if (inp[0].equalsIgnoreCase("pass")) {
            pass.pass(inp, bank);
        } else if (inp[0].equalsIgnoreCase("withdraw")) {
            withdraw.withdraw(inp, bank);
        }
    }
}
