package banking;

public class TransferProcessor {

    protected void transfer(String[] byparts, Bank bank) {
        int ID1 = Integer.parseInt(byparts[1]), ID2 = Integer.parseInt(byparts[2]);
        double cash = Double.parseDouble(byparts[3]);
        bank.transfer(ID1, ID2, cash);
    }
}
