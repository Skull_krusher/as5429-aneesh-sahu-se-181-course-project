package banking;

public class PasstimeProcessor {
    protected void pass(String[] byparts, Bank bank) {
        int num = Integer.parseInt(byparts[1]);

        for (int i = 0; i < num; i++) {
            bank.removeEmptyAccount();
            bank.lowBalanceDeduction();
            bank.applyInterest();
            bank.ageAccount();
        }
    }
}
