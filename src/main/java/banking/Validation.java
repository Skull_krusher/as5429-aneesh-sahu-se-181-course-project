package banking;

public class Validation {
    private Bank bank;
    private CreateValidation createValidation;
    private DepositeValidation depositValidation;
    private TransferValidation transferValidation;
    private PassTimeValidation passTimeValidation;
    private WithdrawValidation withdrawValidation;


    Validation(Bank bank) {
        this.bank = bank;
        this.createValidation = new CreateValidation();
        this.depositValidation = new DepositeValidation();
        this.transferValidation = new TransferValidation();
        this.passTimeValidation = new PassTimeValidation();
        this.withdrawValidation = new WithdrawValidation();
    }

    protected boolean validate(String inp) {
        inp = inp.stripTrailing();
        String[] parts = inp.split(" ");
        switch (parts[0].toUpperCase()) {
            case "CREATE":
                return createValidation.CreateCheck(parts, bank);
            case "DEPOSIT":
                return depositValidation.DepositCheck(parts, bank);
            case "TRANSFER":
                return transferValidation.TransferCheck(parts, bank);
            case "PASS":
                return passTimeValidation.PasTimeCheck(parts);
            case "WITHDRAW":
                return withdrawValidation.WithdrawCheck(parts, bank);
            default:
                return false;
        }
    }


}
