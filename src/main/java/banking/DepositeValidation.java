package banking;

public class DepositeValidation extends ValidationCommons {
    private Bank bank;

    DepositeValidation() {

    }

    protected boolean DepositCheck(String[] byparts, Bank bank) {
        this.bank = bank;
        if (lengthChecker(byparts) && BankHasAccount(byparts[1], bank)) {
            return DepositValueChecker(byparts);
        } else {
            return false;
        }
    }

    private boolean DepositValueChecker(String[] byparts) {
        int id = Integer.parseInt(byparts[1]);
        if (!CheckIfNumeric(byparts[2])) {
            return false;
        }
        double amount = Double.parseDouble(byparts[2]);
        return bank.IsDepositValid(id, amount);
    }

    protected boolean lengthChecker(String[] byparts) {
        return byparts.length == 3;

    }
}
