package banking;

public class CreateProcessor {
    protected void create(String[] inp, Bank bank) {
        if (inp[1].equalsIgnoreCase("savings")) {
            SavingsCheckingCreate(inp, bank, false);
        } else if (inp[1].equalsIgnoreCase("checking")) {
            SavingsCheckingCreate(inp, bank, true);
        } else if (inp[1].equalsIgnoreCase("cd")) {
            CdCreate(inp, bank);
        }
    }

    private void CdCreate(String[] inp, Bank bank) {
        int id = Integer.parseInt(inp[2]);
        double apr = Double.parseDouble(inp[3]);
        double cash = Double.parseDouble(inp[4]);
        accountbase account = new CD(id, apr, cash);
        bank.addAccount(account);

    }

    private void SavingsCheckingCreate(String[] inp, Bank bank, boolean isChecking) {
        int id = Integer.parseInt(inp[2]);
        double apr = Double.parseDouble(inp[3]);
        accountbase account;
        if (isChecking) {
            account = new Checkings(id, apr);
        } else {
            account = new Savings(id, apr);
        }
        bank.addAccount(account);
    }
}
