package banking;

public class CreateValidation extends ValidationCommons {
    private Bank bank;

    CreateValidation() {

    }

    protected boolean CreateCheck(String[] parts, Bank bank) {
        this.bank = bank;
        if (IsSavingsOrChecking(parts)) {
            return SavingsCheckingTest(parts);
        } else if (IsCd(parts)) {
            return CdTest(parts);
        } else {
            return false;
        }
    }

    private boolean IsCd(String[] parts) {
        return lengthChecker(parts) && parts[1].equalsIgnoreCase("cd");
    }

    private boolean IsSavingsOrChecking(String[] parts) {
        return lengthChecker(parts) && (parts[1].equalsIgnoreCase("savings") || parts[1].equalsIgnoreCase("checking"));
    }

    private boolean SavingsCheckingTest(String[] parts) {
        return lengthChecker(parts, 1) && IDChecker(parts[2]) && noDuplicateId(parts, bank) && AprChecker(parts);
    }

    private boolean CdTest(String[] parts) {
        return lengthChecker(parts, 2) && IDChecker(parts[2]) && noDuplicateId(parts, bank) && AprChecker(parts) && CdCreateAmountChecker(parts);
    }

    protected boolean lengthChecker(String[] byparts, int select) {
        if (select == 1) {
            return byparts.length == 4;
        } else {
            return byparts.length == 5;
        }

    }

    protected boolean lengthChecker(String[] byparts) {
        return byparts.length >= 4;

    }
}
