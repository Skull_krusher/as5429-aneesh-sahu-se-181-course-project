package banking;

import java.util.List;

public class MasterControl {
    Bank bank;
    Validation validation;
    CommandProcessor commandProcessor;
    CommandStorage storage;

    MasterControl(Bank bank, Validation validation, CommandProcessor commandProcessor, CommandStorage storage) {
        this.bank = bank;
        this.validation = validation;
        this.commandProcessor = commandProcessor;
        this.storage = storage;
    }

    public List<String> start(List<String> input) {
        for (String command : input) {
            if (validation.validate(command)) {
                commandProcessor.process(command);
                storage.store(command, true);
            } else {
                storage.store(command, false);
            }
        }
        return storage.output();
    }
}
